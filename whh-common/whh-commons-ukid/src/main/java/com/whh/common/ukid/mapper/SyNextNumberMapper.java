package com.whh.common.ukid.mapper;


import com.whh.common.ukid.model.SyNextNumberDO;

public interface SyNextNumberMapper {
    SyNextNumberDO select(String numberType);

    String getNextNumber(SyNextNumberDO nn);

    /**
     * 获取_seq的序列：从100000开始每次增1的数字
     *
     * @return
     */
    public Long getNextSeq();

    public String getAppSecret();
}
