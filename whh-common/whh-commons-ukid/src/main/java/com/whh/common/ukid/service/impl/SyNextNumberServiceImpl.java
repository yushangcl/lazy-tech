package com.whh.common.ukid.service.impl;

import com.whh.common.ukid.mapper.SyNextNumberMapper;
import com.whh.common.ukid.model.SyNextNumberDO;
import com.whh.common.ukid.service.SyNextNumberService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 流水号序列数的实现.
 *
 * @author huahui.wu. (;￢＿￢)
 * Created on 2018/4/19.
 */
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class SyNextNumberServiceImpl implements SyNextNumberService {

    @Resource
    private SyNextNumberMapper syNextNumberMapper;

    @Override
    public String getNextNumber(String numberType) {
        SyNextNumberDO nextNumberDO = new SyNextNumberDO();
        nextNumberDO.setNumberType(numberType);
        syNextNumberMapper.getNextNumber(nextNumberDO);
        return nextNumberDO.getReturnNo();
    }

    @Override
    public Long getNextSeq() {
        return syNextNumberMapper.getNextSeq();
    }

    @Override
    public String getAppSecret() {
        return syNextNumberMapper.getAppSecret();
    }
}
