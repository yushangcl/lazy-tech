package com.whh.common.ukid.service.mock;


import com.whh.common.ukid.service.SyNextNumberService;
import com.whh.common.utils.util.DateUtil;
import com.whh.common.utils.util.StringUtils;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 流水号序列数的实现
 *
 * @author huahui.wu. (;￢＿￢)
 * Created on 2018/4/19.
 */
public class SyNextNumberServiceMock implements SyNextNumberService {

    private static Map<String, NextNumber> nextNumberMap;
    long num = 10000000;

    private synchronized void init() {
        nextNumberMap = new ConcurrentHashMap<>();
        nextNumberMap.put("XU0", new NextNumber("XU", "XU0", 1, 1));
        nextNumberMap.put("XU1", new NextNumber("XU", "XU1", 1, 1));
        nextNumberMap.put("XU2", new NextNumber("XU", "XU2", 1, 1));
        nextNumberMap.put("XU3", new NextNumber("XU", "XU3", 1, 1));
        nextNumberMap.put("XU4", new NextNumber("XU", "XU4", 1, 1));
        nextNumberMap.put("XU5", new NextNumber("XU", "XU5", 1, 1));
        nextNumberMap.put("XU6", new NextNumber("XU", "XU6", 1, 1));
        nextNumberMap.put("XU7", new NextNumber("XU", "XU7", 1, 1));
        nextNumberMap.put("XU8", new NextNumber("XU", "XU8", 1, 1));
        nextNumberMap.put("XU9", new NextNumber("XU", "XU9", 1, 1));
        nextNumberMap.put("BE", new NextNumber("1", "BE", 0, 1));
        nextNumberMap.put("BF", new NextNumber("24", "BF", 0, 1));
        nextNumberMap.put("BH", new NextNumber("25", "BH", 0, 1));
        nextNumberMap.put("BI", new NextNumber("10", "BI", 0, 1));
    }

    @Override
    public String getNextNumber(String numberType) {
        if (nextNumberMap == null) {
            init();
        }

        NextNumber nextNumber = nextNumberMap.get(numberType);
        try {
            StringBuilder strBuilder = new StringBuilder();
            if (nextNumber != null) {
                strBuilder.append(nextNumber.getPre());
            }

            int seqLen = Integer.parseInt(getAppSecret().substring(13, 14));
            int signPosi = Integer.parseInt(getAppSecret().substring(8, 9));
            String sign = getAppSecret().substring(9, 9 + signPosi);
            strBuilder.append(sign);
            if (nextNumber != null && nextNumber.getUseDate() == 1) {
                strBuilder.append(DateUtil.format(new Date(), "yyMMdd"));
            }

            if (nextNumber != null) {
                long seq = ThreadLocalRandom.current().nextLong(num);
                strBuilder.append(StringUtils.lpad(String.valueOf(seq), seqLen, "0"));
            } else {
                strBuilder.append(num);
                num = num + 100;
            }

            return strBuilder.toString();
        } finally {
            if (nextNumber != null) {
                nextNumber.setNextNumber(nextNumber.getNextNumber() + 1);
            }
        }
    }

    @Override
    public Long getNextSeq() {
        return num++;
    }

    @Override
    public String getAppSecret() {
        return "601115002610080121224303";
    }

    private static class NextNumber {
        private String pre;
        private String numberType;
        private int useDate;
        private Integer nextNumber;

        private NextNumber(String pre, String numberType, int useDate, Integer nextNumber) {
            this.pre = pre;
            this.numberType = numberType;
            this.useDate = useDate;
            this.nextNumber = nextNumber;
        }

        public String getPre() {
            return pre;
        }

        public void setPre(String pre) {
            this.pre = pre;
        }

        public String getNumberType() {
            return numberType;
        }

        public void setNumberType(String numberType) {
            this.numberType = numberType;
        }

        public Integer getNextNumber() {
            return nextNumber;
        }

        public void setNextNumber(Integer nextNumber) {
            this.nextNumber = nextNumber;
        }

        public int getUseDate() {
            return useDate;
        }

        public void setUseDate(int useDate) {
            this.useDate = useDate;
        }
    }

    public static void main(String[] a) {
        SyNextNumberServiceMock mock = new SyNextNumberServiceMock();
        System.out.println(Integer.parseInt(mock.getAppSecret().substring(13, 14)));
        System.out.println((mock.getNextNumber("XU0")));
    }
}
